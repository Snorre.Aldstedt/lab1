package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // Main method that runs the game and connects all the other methods
        while(true){
            System.out.println("Let's play round "+roundCounter);
            String humanChoice = userChoice(rpsChoices);
            String computerChoice = pickRandomChoice(rpsChoices);
            String choiceString = "Human chose "+humanChoice+", computer chose "+computerChoice;
            if(checkWinner(humanChoice, computerChoice)){
                System.out.println(choiceString+". Human wins!");
                this.humanScore ++;
            }
            else if (checkWinner(computerChoice, humanChoice)){
                System.out.println(choiceString+". Computer wins!");
                this.computerScore ++;
            }
            else{
                System.out.println(choiceString+". It's a tie!");
            }
            System.out.println("Score: human "+this.humanScore+", computer "+this.computerScore);
            if (continueGame().equals("n")){
                break;
            }
            this.roundCounter ++;
        }
        System.out.println("Bye bye :)");
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    /**
     * 
     * @param choices
     * @return returns rock/paper/scissors from the player
     */
    public String userChoice(List<String> choices){
        while(true){
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if (validateChoice(humanChoice, choices)) {
                return humanChoice;
            }
            else{
                System.out.println("I do not understand "+humanChoice+". Could you try again?");
            }
        }

    }

    /**
     * Picks a random item in a given List of strings
     * @param choices
     * @return random choice from List choices
     */
    public String pickRandomChoice(List<String> choices){
        Random rand = new Random();
        int randomIndex = rand.nextInt(choices.size());
        String randomChoice = choices.get(randomIndex);
        return randomChoice;
    }

    /**
     * Validate that a string is in a list of strings
     * @param choice
     * @param choices
     * @return boolean value that checks if it is a valid choice given a list
     */
    public boolean validateChoice(String choice, List<String> choices){
        boolean validation = choices.contains(choice);
        return validation;
    }
    /**
     * 
     * @return returns y or n
     */
    public String continueGame(){
        while(true) {
            String continueAnswer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (validateChoice(continueAnswer, Arrays.asList("y","n"))){
                return continueAnswer;
            }
            else{
                System.out.println("I do not understand "+continueAnswer+". Could you try again?");
            }
        }
    }
    /**
     * Check if choice one won
     * @param choiceOne rock, paper or scissors
     * @param choiceTwo rock, paper or scissors
     * @return compares choice one vs choice two and returns if choice one won
     */
    public boolean checkWinner(String choiceOne, String choiceTwo){
        if(choiceOne.equals("paper")){
            boolean resultBoolean = choiceTwo.equals("rock");
            return resultBoolean;
        }
        else if(choiceOne.equals("rock")){
            boolean resultBoolean = choiceTwo.equals("scissors");
            return resultBoolean;
        }
        else if(choiceOne.equals("scissors")){
            boolean resultBoolean = choiceTwo.equals("paper");
            return resultBoolean;
        }
        else{
            return false;
        }
    }

}

